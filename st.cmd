require essioc
require lodistribution

# Load standard module startup scripts
iocshLoad("$(essioc_DIR)/common_config.iocsh")

epicsEnvSet("PREFIX",      "TS2-010RFC:RFS-LOM-201")
epicsEnvSet("DEVICE_NAME", "/dev/xdma9")
epicsEnvSet("PORT",        "LOPort")

# Load lodistribution startup script
iocshLoad("$(lodistribution_DIR)/lo.iocsh", "P=$(PREFIX):, R=")

# Load archiver configuration
dbLoadTemplate("$(E3_CMD_TOP)/archiver-config.substitutions", "P=$(PREFIX):, R=")

